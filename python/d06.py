#! //usr/bin/python3
"""Day 06 of Advent of Code"""
import advent

DAY = 6
DELIMITER = "\n\n"
FUNC = lambda group_text: [set(line) for line in group_text.split("\n")]


def get_args():
    """Gets arguments from command line"""
    return advent.default_get_args()


def main():
    """Return outputs for today's puzzle.
    Assumes input has already been downloaded and saved in inputs folder"""
    args = get_args()
    puzzle_input = advent.get_input(DAY, delimiter=DELIMITER, func=FUNC)

    def count_all_distinct_entries(input_list):
        if not input_list:
            return 0
        first_set = input_list[0]
        return len(first_set.union(*input_list))

    def count_all_entries_shared_by_all(input_list):
        if not input_list:
            return 0
        first_set = input_list[0]
        return len(first_set.intersection(*input_list))

    output1 = sum([count_all_distinct_entries(group) for group in puzzle_input])
    output2 = sum([count_all_entries_shared_by_all(group) for group in puzzle_input])

    advent.print_output(output1, output2, args)
    assert output1 == 6735
    assert output2 == 3221


if __name__ == "__main__":
    main()
