#! //usr/bin/python3
"""Day 00 of Advent of Code"""
import pprint
import sys
import advent
from advent import eprint

DAY = 0
DELIMITER = "\n"
FUNC = None
# FUNC = lambda x: int(x)


def get_args():
    """Gets arguments from command line"""
    return advent.default_get_args()


def main():
    """Return outputs for today's puzzle.
    Assumes input has already been downloaded and saved in inputs folder"""
    args = get_args()
    puzzle_input = advent.get_input(DAY, delimiter=DELIMITER, func=FUNC)

    eprint("Puzzle Input:")
    pprint.pprint(puzzle_input, stream=sys.stderr)

    output1 = "XXXX"
    output2 = "XXXX"

    advent.print_output(output1, output2, args)


if __name__ == "__main__":
    main()
