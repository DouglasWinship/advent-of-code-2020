#! //usr/bin/python3
"""Day 05 of Advent of Code"""
import advent

DAY = 5
DELIMITER = "\n"
FUNC = lambda code_string: seat_id_from_code(code_string)  # pylint: disable=W0108


def get_args():
    """Gets arguments from command line"""
    return advent.default_get_args()


def seat_id_from_code(code_string):
    """Gets seat id from code string"""
    seat_num_tuple = seat_from_code(code_string)
    return 8 * seat_num_tuple[0] + seat_num_tuple[1]


def seat_from_code(code_string):
    """Get seat number (as a row, seat tuple) from the 10 letter code string"""
    return (num_from_code(code_string[6::-1]), num_from_code(code_string[:6:-1]))


def num_from_code(code_string):
    """Get a binary number from a string of Fs and Bs, or Rs and Ls.
    Assumes that B and R represent 1, and anything else represents 0.
    Assumes that the number has been reversed, with the lowest value digits on the
    left"""
    return sum(
        [pow(2, i) for i, letter in enumerate(code_string) if letter in ["B", "R"]]
    )


def main():
    """Return outputs for today's puzzle.
    Assumes input has already been downloaded and saved in inputs folder"""
    args = get_args()
    puzzle_input = advent.get_input(DAY, delimiter=DELIMITER, func=FUNC)
    puzzle_input.sort()

    output1 = max(puzzle_input)
    all_possible_seat_ids = set(range(min(puzzle_input), max(puzzle_input) + 1))
    output2 = max(all_possible_seat_ids.difference(puzzle_input))

    advent.print_output(output1, output2, args)
    assert output1 == 858
    assert output2 == 557


if __name__ == "__main__":
    main()
