#! //usr/bin/python3
"""Day 04 of Advent of Code"""

import re
import advent

DAY = 4
DELIMITER = "\n\n"
FUNC = lambda raw_passport: parse_passport(raw_passport)  # pylint: disable=W0108


EXPECTED_KEYS = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
OPTIONAL_KEYS = {"cid"}


def parse_passport(raw_passport):
    """Process a passport string into a passport dictionary."""
    passport = {}
    for raw_field_string in raw_passport.split():
        key, value = raw_field_string.split(":")
        passport[key] = value
    return passport


def get_args():
    """Gets arguments from command line"""
    return advent.default_get_args()


def validate_hgt(value):
    """Validates that a height string is valid,
    according to the rules of today's puzzle"""
    match = re.match(r"^(\d+)(cm|in)", value)
    if match:
        if match.group(2) == "cm":
            return 150 <= int(match.group(1)) <= 193
        if match.group(2) == "in":
            return 59 <= int(match.group(1)) <= 76
    return False


def validate_passport(passport, validate_values=True):
    """Validates a passport dictionary, according to the rules of today's puzzle"""
    keys_found = set(passport.keys())
    if EXPECTED_KEYS.difference(keys_found):
        # If any of the expected keys aren't present in the passport
        return False
    if validate_values:
        for key, func in {
            "byr": lambda val: 1920 <= int(val) <= 2002 and re.match(r"^\d{4}$", val),
            "iyr": lambda val: 2010 <= int(val) <= 2020 and re.match(r"^\d{4}$", val),
            "eyr": lambda val: 2020 <= int(val) <= 2030 and re.match(r"^\d{4}$", val),
            "hgt": validate_hgt,
            "hcl": lambda val: bool(re.match(r"#[\da-f]{6}", val)),
            "ecl": lambda val: val in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
            "pid": lambda val: bool(re.match(r"^[\d]{9}$", val)),
        }.items():
            if not func(passport[key]):
                return False
    return True


def main():
    """Return outputs for today's puzzle.
    Assumes input has already been downloaded and saved in inputs folder"""
    args = get_args()
    puzzle_input = advent.get_input(DAY, delimiter=DELIMITER, func=FUNC)

    output1 = 0
    output2 = 0
    for passport in puzzle_input:
        output1 += 1 if validate_passport(passport, validate_values=False) else 0
        output2 += 1 if validate_passport(passport, validate_values=True) else 0

    advent.print_output(output1, output2, args)
    assert output1 == 202
    assert output2 == 137


if __name__ == "__main__":
    main()
