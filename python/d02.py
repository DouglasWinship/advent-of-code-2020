#! //usr/bin/python3
"""Day 02 of Advent of Code"""
import re
from collections import namedtuple
import advent

# from advent import eprint

DAY = 2
DELIMITER = "\n"
FUNC = None

MATCH_EXPRESSION = re.compile(
    r"""
        (?P<lower>\d+)        # one or more digits
        -                     # hypen
        (?P<upper>\d+)        # one or more digits
        \s                    # whitespace
        (?P<character>\w)     # character
        :\s                   # colon and whitespace
        (?P<password>\w+)     # password
        """,
    re.VERBOSE,
)

MatchData = namedtuple("MatchData", ["lower", "upper", "character", "password"])


def get_match(password_line):
    """Gets a namedtuple Match Data, from a password string"""
    re_match = re.match(MATCH_EXPRESSION, password_line)
    return MatchData(
        lower=int(re_match.group("lower")),
        upper=int(re_match.group("upper")),
        character=re_match.group("character"),
        password=re_match.group("password"),
    )


def valid_password_1(match):
    """Checks whether password information is valid, based on today's first rule"""
    count = match.password.count(match.character)
    return match.lower <= count <= match.upper


def valid_password_2(match):
    """Checks whether password information is valid, based on today's first rule"""

    first_char = match.password[match.lower - 1]  # -1 because of no zero-indexing
    second_char = match.password[match.upper - 1]  # -1 because of no zero-indexing
    first_char_match = bool(first_char == match.character)
    second_char_match = bool(second_char == match.character)
    return first_char_match != second_char_match  # Exactly one must be true


def get_args():
    """Gets arguments from command line"""
    return advent.default_get_args()


def main():
    """Return outputs for today's puzzle.
    Assumes input has already been downloaded and saved in inputs folder"""
    args = get_args()
    puzzle_input = advent.get_input(DAY, delimiter=DELIMITER, func=FUNC)

    output1 = 0
    output2 = 0

    for line in puzzle_input:
        match = get_match(line)
        output1 += 1 if valid_password_1(match) else 0
        output2 += 1 if valid_password_2(match) else 0

    advent.print_output(output1, output2, args)
    assert output1 == 469
    assert output2 == 267


if __name__ == "__main__":
    main()
