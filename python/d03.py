#! //usr/bin/python3
"""Day 03 of Advent of Code"""
import advent
from advent import eprint

DAY = 3
DELIMITER = "\n"
FUNC = None


def get_args():
    """Gets arguments from command line"""
    return advent.default_get_args()


def main():
    """Return outputs for today's puzzle.
    Assumes input has already been downloaded and saved in inputs folder"""
    args = get_args()
    puzzle_input = advent.get_input(DAY, delimiter=DELIMITER, func=FUNC)

    height = len(puzzle_input)
    width = len(puzzle_input[0])

    def count_trees(h_step, v_step):
        h_pos = 0
        v_pos = 0
        tree_count = 0
        while v_pos < height:
            tree_count += 1 if puzzle_input[v_pos][h_pos % width] == "#" else 0
            # print(f"v_pos: {v_pos}")
            # print(f"h_pos: {h_pos}")
            # print(f"h_pos % width: {h_pos % width}")
            # print(f"puzzle_input[v_pos][h_pos % width]: {puzzle_input[v_pos][h_pos % width]}")
            # print(f"tree_count: {tree_count}")
            h_pos += h_step
            v_pos += v_step
        return tree_count

    product = 1
    for ratio in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
        count = count_trees(ratio[0], ratio[1])
        eprint(f"Right {ratio[0]}, Down {ratio[1]}, Found {count} trees")
        product *= count

    output1 = count_trees(h_step=3, v_step=1)
    output2 = product

    advent.print_output(output1, output2, args)
    assert output1 == 218
    assert output2 == 3847183340


if __name__ == "__main__":
    main()
