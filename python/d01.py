#! //usr/bin/python3
"""Day 01 of Advent of Code"""
# import pprint
import advent
from advent import eprint

DAY = 1
DELIMITER = "\n"
FUNC = lambda x: int(x)  # pylint: disable=W0108


def get_args():
    """Gets arguments from command line"""
    return advent.default_get_args()


def find_nums_that_sum(input_list, goal_sum, count=1):
    """Inspects a list, for a group of {count} entries, which add up to {goalsum}
    Assumes the list will consist of integers"""
    if count == 1:
        if goal_sum in input_list:
            return [goal_sum]
        return None
    next_count = count - 1
    copy_list = input_list.copy()
    while copy_list:
        first_num = copy_list.pop(0)
        target_sum = goal_sum - first_num
        search_result = find_nums_that_sum(copy_list, target_sum, count=next_count)
        if search_result:
            return [first_num] + search_result
    return None


def main():
    """Return outputs for today's puzzle.
    Assumes input has already been downloaded and saved in inputs folder"""
    args = get_args()
    puzzle_input = advent.get_input(DAY, delimiter=DELIMITER, func=FUNC)

    first_result = find_nums_that_sum(puzzle_input, 2020, count=2)
    second_result = find_nums_that_sum(puzzle_input, 2020, count=3)
    if first_result:
        output1 = first_result[0] * first_result[1]
        eprint(f"First result: {first_result}")
    else:
        eprint("First result not found")
    if second_result:
        output2 = second_result[0] * second_result[1] * second_result[2]
        eprint(f"Second result: {second_result}")
    else:
        eprint("Second result not found")

    advent.print_output(output1, output2, args)
    assert output1 == 989824
    assert output2 == 66432240


if __name__ == "__main__":
    main()
