"""
Utility functions for my attempts at Advent Of Code, 2020
This is a place to store any functions or code that I think I'm likely to re-use.
This may be split into more files as the event continues.

Advent of code: https://adventofcode.com
"""
import argparse
import pprint
import sys


def hello_santa():
    """Christmas-themed Greeting"""
    eprint("Hello Santa\n\n")


def eprint(item):
    """Print to stderr"""
    print(item, file=sys.stderr)


def get_input(day_num, delimiter=None, func=None):
    """Fetches the day's puzzle input, based on the day number"""
    filename = f"../inputs/{day_num:02d}"
    with open(filename, mode="r") as readfile:
        content = readfile.read()
        if not delimiter:
            return content
        content_list = content.rstrip("\n").split(delimiter)
        if not func:
            return content_list
        return [func(entry) for entry in content_list]


def default_get_args():
    """Gets arguments from command line"""
    arg_parser = argparse.ArgumentParser(description="Finds output for Advent of Code")
    arg_parser.add_argument("--output1", action="store_true", help="return output 1")
    arg_parser.add_argument("--output2", action="store_true", help="return output 2")
    return arg_parser.parse_args()


def print_output(output1, output2, args):
    """Prints output(s)"""
    if args.output1:
        print(output1)
    elif args.output2:
        print(output2)
    else:
        pprint.pprint({"Output 1": output1, "Output 2": output2})
